# Penny Pig
An android application to keep track of expenses and income of an individual. This project also provides spliting of expenses between multiple persons.

The basic objective of this application is to help the user manage their currency in a more efficient way. The user
can add the details of their income and expense and bifurcate them in various sections. They can create different
accounts (eg. Cash, Bank etc) to manage different depositories. Penny Pig also helps the user bifurcate their
expenses in different categories which can help the user to analyze their day to day finances. One major feature of
Penny Pig is that it can manage and track the amount related to different people. User can create different groups
or deal with a person directly based on the amount they paid or they need to pay. This application is one-stop
expense management which can help you work out your spending habits and get you on an economical track.
